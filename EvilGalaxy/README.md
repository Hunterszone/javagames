# README - check it out!

### Main techniques: moving sprites, collision detection, 3 levels of difficulty, multiple levels, AI algorithms

## HOW TO LAUNCH, USING LAUNCHER:

Create an empty folder and download the Launcher.jnlp file into it, using the [![button](https://java.com/js/webstart.png)](https://me4gaming.com/LauncherEG/Launcher.jnlp) button.  
After executing the file, it will automatically download all the required stuff inside the dir 
and will launch the game immediately after that.  
It will also compare the game version and will update it, if needed.

NB.: Make sure you add http://me4gaming.com and https://me4gaming.com to the Java Exception Site List, using this article: https://www.java.com/en/download/faq/exception_sitelist.xml .  
And if you later struggle to run the JNLP application, make sure you've granted all permission within your 

```java
<jre location>\lib\security\java.policy
```
file like this:
  
```java
grant {
  permission java.security.AllPermission;
};
```
Otherwise just download the EGinit.jar into an empty folder from [here](https://github.com/Hunterszone/EvilGalaxy/blob/master/EGinit.jar?raw=true) (NB: some browsers could deny the operation!)

## HOW TO INSTALL/LAUNCH (manually):

### *For WINDOWS users:*   
Launch the game from the EvilGalaxy.exe WITHIN its folder or from a shortcut on your desktop. If you remove the .exe file outside the folder, the game resources won't be found, which will result in a failure to execute the game.

### *For NON-WINDOWS users:*   
Launch the game from the EvilGalaxy.jar WITHIN its folder or from a shortcut on your desktop. If you remove the .jar file outside the folder, the game resources won't be found, which will result in a failure to execute the game.



## MANUAL: 

Use the ARROWS to navigate your ship.   
Use 'SPACE' and 'CTRL' to fire, depends on which weapon is unlocked.  
Use 'S' to mute or 'A' to enable the background music.  
Use 'P' to pause the game.  
Use 'R' to restart the game when in running state.  
Use 'E' to restart/resume if not in running state, or switch to E A S Y.  
Use 'M' to restart/resume if not in running state, or switch to M E D I U M.  
Use 'H' to restart/resume if not in running state, or switch to H A R D.  
Use 'O' to open the game manual.  
Use 'G' to turn ON or OFF G O D M O D E.  
Use keys '1' to '4' to switch the level.  
Use Alt+Z to instantly save the game state, Alt+X to enable auto-saving and Alt+C to load already saved game.   
Use 'ESC' to quit the game.  



## AUTHOR: 

Konstantin Drenski


*ENJOY RESPONSIBLY! :)*
